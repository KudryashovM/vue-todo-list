import Vue from 'vue'
import Vuex from 'vuex'
import db from './firebase'
import firebase from 'firebase/app'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    tasks: [],
    tasksIndex: [],
    page: 1,
    semaphore: 0
  },
  getters: {
    getTasks (state) {
      return state.tasks
    },
    availablePages (state) {
      return Math.ceil(state.tasksIndex.length / 10)
    },
    isLoading (state) {
      return state.semaphore > 0
    }
  },
  mutations: {
    updateTask (state, payload) {
      let tasks = state.tasks
      let task = tasks.find(task => task.id === payload.id)
      task.label = payload.label
      task.description = payload.description
    },
    loadTasks (state, tasks) {
      state.tasks = tasks
    },
    initTasksIndex (state, tasksIndex) {
      state.tasksIndex = tasksIndex
    },
    nextPage (state) {
      state.page++
    },
    prevPage (state) {
      state.page--
    },
    setPage (state, page) {
      state.page = page
    },
    changeTaskState (state, task) {
      task.isDone = !task.isDone
    },
    startLoading (state) {
      return state.semaphore++
    },
    finishLoading (state) {
      //dumb way to prevent preloader blinking
      setTimeout(function () {
        return state.semaphore--
      }, 500)
    }
  },
  actions: {
    loadNextPage ({commit}) {
      commit('nextPage')
    },

    loadPrevPage ({commit}) {
      commit('prevPage')
    },

    async getTasksFromDB ({commit}) {
      if (!this.state.tasksIndex.length) {
        return
      }
      commit('startLoading')
      let newTasks = []
      await db.collection('tasks')
        .orderBy('createdAt', 'desc')
        .startAt(Object.values(this.state.tasksIndex)[(this.state.page - 1) * 10])
        .limit(10)
        .get()
        .then(function (querySnapshot) {
          querySnapshot.forEach(function (doc) {
            let task = {
              id: doc.id,
              label: doc.data().label,
              description: doc.data().description,
              createdAt: doc.data().createdAt,
              isDone: doc.data().isDone
            }
            newTasks.push(task)
          })
          commit('loadTasks', newTasks)
        })
      commit('finishLoading')
    },

    async updateTaskFromDB ({commit, state}, updatedTask) {
      commit('startLoading')
      await db.collection('tasks').doc(updatedTask.id)
        .update({
          label: updatedTask.label,
          description: updatedTask.description
        })
      commit('updateTask', updatedTask)
      commit('finishLoading')
    },

    async addTaskToDB ({commit, state}, taskLabel) {
      commit('startLoading')
      const createdAt = firebase.firestore.Timestamp.fromDate(new Date())
      await db.collection('tasks').add({
        label: taskLabel,
        description: '',
        createdAt: createdAt,
        isDone: false
      })
      commit('finishLoading')
    },

    async removeTaskFromDB ({commit, state}, taskId) {
      commit('startLoading')
      await db.collection('tasks').doc(taskId).delete()
      commit('finishLoading')
    },

    async loadTasksIndex ({commit}) {
      commit('startLoading')
      const dateCollection = []
      const collection = await db.collection('tasks').orderBy('createdAt', 'desc').get()
      collection.docs.forEach(doc => {
        dateCollection.push(doc.data().createdAt)
      })
      commit('initTasksIndex', dateCollection)
      commit('finishLoading')
    },

    async changeTaskState ({commit}, taskId) {
      commit('startLoading')
      const task = this.state.tasks.find(item => item.id === taskId)
      await db.collection('tasks').doc(taskId)
        .update({
          isDone: !task.isDone
        })
      commit('changeTaskState', task)
      commit('finishLoading')
    }
  }
})
