import Vue from 'vue'
import Router from 'vue-router'
import TasksListView from '../components/TasksListView'
import TasksEditView from '../components/TaskEditView'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '*',
      redirect: '/page/1'
    },
    {
      path: '/page/:pageNum',
      name: 'TasksListView',
      component: TasksListView
    },
    {
      path: '/edit/:id',
      name: 'TasksEditView',
      component: TasksEditView
    }
  ]
})
