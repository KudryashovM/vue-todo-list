import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/database'
import 'firebase/storage'

const options = {
  projectId: 'vue-studying-db',
  databaseURL: 'https://vue-studying-db.firebaseio.com'
}
const db = firebase.initializeApp(options).firestore()

export default db
