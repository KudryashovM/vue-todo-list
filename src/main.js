import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'

new Vue({
  el: '#app',
  router,
  store,
  components: {App},
  template: '<App/>',
  async created () {
    await this.$store.dispatch('loadTasksIndex')
    await this.$store.dispatch('getTasksFromDB')
  }
})
